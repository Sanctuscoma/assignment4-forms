/**
 * Created by theotheu on 09-10-14.
 */
module.exports = {
    development: {
        debug: true,                           // set debugging on/off
        db: 'mongodb://localhost/cria-dev',    // change with your database
        port: 3000                             // change 3000 with your port number
    }, test: {
        debug: false,                          // set debugging on/off
        db: 'mongodb://localhost/cria-test',   // change with your database
        port: 1300                             // change 1300 with your port number
    }, production: {

    }
};